Hello, my name is Mikołaj, and here's how to run the code:

You need to have node.js and git downloaded:  
node.js - https://nodejs.org/en/download/  
git - https://git-scm.com/downloads

You need to install both of the tools. You also should log into your gitlab account.
To clone the code you should paste this command to git:  
`git clone git@gitlab.com:JalokimFreelance/tea-code-challenge.git`  
and then:  
`cd tea-code-challenge `  
Once you are inside the folder, you should write:  
`npm install`  
to install all needed node modules.  
To run the code in the browser you just need to write:  
`npm start`
