import { useState } from "react";
const User = ({ user, onClick, selectedUsers }) => {
  const [selected, setSelected] = useState(selectedUsers.includes(user.id)); //check if user was selected before

  return (
    <div
      onClick={() => {
        onClick(user.id);
        setSelected(!selected);
      }}
      style={selected ? selectedStyle : styling}
    >
      <img alt="" src={user.avatar}></img>

      {`${user.first_name} ${user.last_name}      `}

      <input readOnly type="checkbox" checked={selected}></input>
    </div>
  );
};

//styling
const styling = {
  border: "1px gray solid",
  margin: "10px",
  backgroundColor: "rgb(237, 236, 232)",
};

const selectedStyle = {
  ...styling,
  backgroundColor: "green",
};

export default User;
