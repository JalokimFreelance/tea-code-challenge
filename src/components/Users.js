import User from "./User";
const Users = ({ users, onClick, selectedUsers }) => {
  return (
    <div>
      {users.map((user) => {
        return (
          <User
            user={user}
            key={user.id}
            onClick={onClick}
            selectedUsers={selectedUsers}
          />
        );
      })}
    </div>
  );
};

export default Users;
