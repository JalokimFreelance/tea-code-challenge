const SearchBar = ({ onChange }) => {
  return (
    <div style={styling}>
      <input
        type="text"
        placeholder="Search for people"
        style={{ width: "100%" }}
        onChange={(e) => {
          onChange(e.target.value);
        }}
      />
    </div>
  );
};

//styling
const styling = {
  width: "100%",
  padding: "10px",
};

export default SearchBar;
