import "./App.css";
import { useEffect, useState } from "react";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import Users from "./components/Users";

function App() {
  const [users, setUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [searchedUsers, setSearchedUsers] = useState([]);
  const [userSearched, setUserSearched] = useState(false);

  //fetch users from API
  const fetchUsers = async () => {
    const res = await fetch(
      "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    );
    const data = await res.json();
    return data;
  };

  //get users from API when the site loads
  useEffect(() => {
    const getUsers = async () => {
      const fetchedUsers = await fetchUsers();
      fetchedUsers.sort((user1, user2) => {
        if (user1.last_name > user2.last_name) {
          return 1;
        }
        if (user1.last_name < user2.last_name) {
          return -1;
        }
        return 0;
      });
      setUsers(fetchedUsers);
    };
    getUsers();
  }, []);

  //selects the user
  const selectUser = (id) => {
    //check if the user is already selected
    let index = selectedUsers.indexOf(id);

    //splice or push depending on if user was selected before
    let tempArray = [];
    if (index === -1) {
      tempArray = selectedUsers;
      tempArray.push(id);
    } else {
      tempArray = selectedUsers;
      tempArray.splice(index, 1);
    }

    //sort the selected users array
    tempArray.sort((a, b) => a - b);

    //update state
    setSelectedUsers(tempArray);

    //console log users ids
    console.log("Selected users:");
    selectedUsers.forEach((user) => {
      console.log(user);
    });
  };

  const searchUser = (string) => {
    //check if user is trying to search for someone
    setUserSearched(string.length > 0);

    //if not, stop the function
    if (!(string.length > 0)) {
      return;
    }

    //trim and split the value from search box
    let arrayOfStrings = string.trim().split(" ");

    //create a temporary array to store users
    let tempArray = users;

    //check if each part from search box matches the user string
    for (let i = 0; i < arrayOfStrings.length; i++) {
      let re = new RegExp(arrayOfStrings[i], "i");
      tempArray = tempArray.filter((user) =>
        re.test(user.first_name + " " + user.last_name)
      );
    }

    //update searched users
    setSearchedUsers(tempArray);
  };

  //return app component
  return (
    <div className="App">
      <Header text="Contacts" />
      <SearchBar onChange={searchUser} />
      <Users
        users={userSearched ? searchedUsers : users}
        onClick={selectUser}
        selectedUsers={selectedUsers}
      />
    </div>
  );
}

export default App;
